import Head from 'next/head'
import styles from './layout.module.css'
import Link from 'next/link'
import Header from "./header";
import Footer from "./footer";

const defaultName = 'Mon site'
export const defaultSiteTitle = 'Mon magnifique site'

export default function Layout({ children, home, name = null, siteTitle = null }) {
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Exemple de site pouvant êre monté rapidement dans le cadre d'un stage"
        />
        <meta
          property="og:image"
          content={`https://og-image.vercel.app/${encodeURI(
            siteTitle ?? defaultSiteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle ?? defaultSiteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <Header home={home} siteName={name ?? defaultName}></Header>
      <main>{children}</main>
      {!home && (
        <div className={styles.backToHome}>
          <Link href="/">← Retour à l'accueil</Link>
        </div>
      )}
      <Footer siteName={siteTitle ?? defaultSiteTitle}></Footer>
    </div>
  )
}
