import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import {Carousel} from 'react-responsive-carousel';
import styles from './custom-carousel.module.css'

//https://www.npmjs.com/package/react-responsive-carousel
export default function CustomCarousel({imgs = []}) {
  return (
    <div className={styles.zone}>
      <Carousel autoPlay={true} showThumbs={false}>
        <div>
          <img src="/images/carousel/league-1.jpeg"/>
          <p className="legend">Legend 1</p>
        </div>
        <div>
          <img src="/images/carousel/league-2.jpeg"/>
          <p className="legend">Legend 2</p>
        </div>
        <div>
          <img src="/images/carousel/league-3.jpeg"/>
          <p className="legend">Legend 3</p>
        </div>
        <div>
          <img src="/images/carousel/league-4.jpeg"/>
          <p className="legend">Legend 4</p>
        </div>
      </Carousel>
    </div>
  );
};