export default function ImgText({url, content, className = ''}) {
  return (
    <div className={className}>
      <img src={url} alt={content} className={className}/>
      {content && content !== '' && <p>{content}</p>}
    </div>
  )
}