Ceci est un projet reposant sur le framework [Next.js](https://nextjs.org/) à destination des stagiaires de 3° lors de leur stage chez Octo.

## Utiliser un IDE pour modifier le projet

1. Pour pouvoir développer, il vous faut un logiciel nommé un IDE. Pour ce tutoriel, nous utiliserons Visual Studio Code. 
Pour l'installer, il faut lancer la commande suivante dans un terminal :
```bash
  brew install --cask visual-studio-code
```

## Comment obtenir le projet ? 

1. Pour commencer, il faut réaliser un "Fork" de ce projet, pour cela il suffit de cliquer sur le bouton en haut à droite de la page de ce projet qui se nomme "Fork".
La création d'un compte est obligatoire si vous n'en avez pas.

2. Pour obtenir ce projet sur votre machine, il vous suffira de faire un "clone" de ce dernier. C'est le bouton bleu qui se trouve sur cette page. 
Dans la liste des choix qui vous sera proposée, vous devez choisir "Visual Studio Code (HTTPS)".
L'éditeur s'ouvrira alors directement avec le projet.

!!! Félicitations, le projet est maintenant sur votre machine !!!

## Comment le démarrer ?

1. Il faut installer la dernière version de [Node.js](https://nodejs.org/) avec la commande suivante, si vous ne l'avez pas déjà :
Pour vérifier si vous l'avez, il faudra taper dans un "terminal", celle commande : 
```bash
  node -v
```

Si rien ne s'affiche alors il faudra l'installer avec la commande suivante :
```bash
  brew install node
```

2. Le projet a besoin d'autres librairies pour fonctionner, pour les installer il faudra utiliser la commande suivante : 
```bash
  npm install
```

3. Pour finir, il ne vous suffira plus qu'à lancer le projet :
```bash
  npm run dev
```

!!! Félicitations le projet est lancé et vous pouvez ouvrir dans votre navigateur l'url suivante [http://localhost:3000](http://localhost:3000) pour voir le site.


## Comment le modifier ?

Toutes les modifications faites dans Visual Studio code sont automatiquement prises en compte sur le site affiché dans votre navigateur.
Vous pouvez commencer par modifier la page `pages/index.js` et voir l'affichage changer dans votre navigateur.


## Pour rendre le site accessible à tous 

La façon la plus simple pour mettre à disposition de tous votre site et de pouvoir le consulter sur votre téléphone est d'utiliser la plateforme [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme).

1. Une fois sur le site de Vercel, il vous suffit de vous connecter en utilisant votre compte Gitlab. C'est le bouton violet sur le formulaire de login. 
2. Puis de sélectionner votre version du projet dans la liste qu'il vous proposera en cliquant sur le bouton "Import".
3. Il vous proposera alors un nom de projet que vous pouvez personnaliser. Et il ne vous restera qu'à cliquer sur "Deploy" en bleu.
4. L'écran va alors se mettre à jour avec les différentes étapes pour déployer votre site.
5. Une fois d'éployé, il ne restera plus qu'à cliquer sur l'image de votre site pour y accéder. 

!!! Félicitations, votre site peut être consulté par tout le monde !!!

## Un peu de documentation

La documentation est disponible sur les sites suivants :

- [Next.js Documentation](https://nextjs.org/docs) - documentation sur les fonctionnalité Next.js et sur l'API
- [Learn Next.js](https://nextjs.org/learn) - un tutoriel interactif sur Next.js

