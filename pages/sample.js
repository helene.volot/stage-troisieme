import Head from 'next/head'
import Layout, {siteTitle} from '../components/layout'
import CustomButton from "../components/custom-button";
import ImgText from "../components/img-text";
import Text from "../components/text";
import Loader from "../components/loader";
import TextBloc from "../components/TextBloc";
import StarWarsEffect from "../components/star-wars-effect";
import CustomCarousel from "../components/custom-carousel";
import {faGamepad, faHammer, faCoffee} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "next/link";

export default function Sample({}) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <div>
        <section>
          <h2>Un bouton</h2>
          <CustomButton name={'mon bouton primaire'} color={null}/>
          <CustomButton name={'mon bouton secondaire'} color={'btn-secondary'}/>
          <CustomButton name={'mon bouton : Success'} color={'btn-success'}/>
          <CustomButton name={'mon bouton : Danger'} color={'btn-danger'}/>
          <CustomButton name={'mon bouton : Attention'} color={'btn-warning'}/>
          <CustomButton name={'mon bouton : Information'} color={'btn-info'}/>
          <CustomButton name={'mon bouton : Brillant'} color={'btn-light'}/>
          <CustomButton name={'mon bouton : Sombre'} color={'btn-dark'}/>
          <CustomButton name={'mon bouton : Lien'} color={'btn-link'}/>
        </section>
        <section>
          <h2>Des Images avec du texte</h2>
          <ImgText url={'images/bob.webp'} content={'Lorem Ipsum'}/>
        </section>
        <section>
          <h2>Du texte</h2>
          <Text content={'Lorem Ipsum'}/>
        </section>
        <section>
          <h2>Du texte avec un titre</h2>
          <TextBloc title={'Title'} content={'Lorem Ipsum'}/>
        </section>
        <section>
          <h2>Un loader</h2>
          <Loader/>
        </section>
        <section>
          <h2>Star Wars prompt</h2>
          <StarWarsEffect title={'A New Hope'} description={'Episode IV'}
                          content={['It is a period of civil war. Rebel spaceships, striking from a hidden base, have won their first victory',
                            'against the evil Galactic Empire.', 'During the battle, Rebel spies managed to steal secret plans to the Empire’s ultimate weapon, the DEATH',
                            'STAR, an armored space station with enough power to destroy an entire planet.', 'Pursued by the Empire’s sinister agents, Princess Leia races home aboard her starship, custodian of the stolen plans that can save her people and restore freedom to the galaxy….']}/>
        </section>
        <section>
          <h2>un carousel</h2>
          <CustomCarousel/>
        </section>
        <section>
          <h2>Des Îcones</h2>
          <div>
            <FontAwesomeIcon icon={faGamepad}/>
            <FontAwesomeIcon icon={faHammer} scale={32}/>
            <FontAwesomeIcon icon={faCoffee} scale={64}/>
          </div>
        </section>
        <Link href={'/'}>Retour à l'accueil</Link>
      </div>
    </Layout>
  )
}
